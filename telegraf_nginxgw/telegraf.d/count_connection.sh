#!/bin/sh
PORT=19098
IP=""
NAME="count_connection"
# Parse parameters
while [ $# -gt 0 ]; do
    case "$1" in
        -p | --port)
                shift
                PORT=$1
                ;;
        -h | --ip)
                shift
                IP=$1
                ;;
		-d | --direction)
                shift
                DIRECTION=$1
                ;;
        -n | --name)
                shift
                NAME=$1
                ;;
        *)  echo "Unknown argument: $1"
            exit 1
            ;;
        esac
shift
done

#Get port connection
CONNECTIONS1=(`ss -H -nt state established $DIRECTION $IP:$PORT | wc -l`)
CONNECTIONS2=(`ss -H -nt state time-wait $DIRECTION $IP:$PORT | wc -l`)
CONNECTIONS3=(`ss -H -nt state close-wait $DIRECTION $IP:$PORT | wc -l`)

echo "$NAME,ip=$IP,port=$PORT,perfdata=Established value=$CONNECTIONS1"
echo "$NAME,ip=$IP,port=$PORT,perfdata=Time_Wait value=$CONNECTIONS2"
echo "$NAME,ip=$IP,port=$PORT,perfdata=Close_Wait value=$CONNECTIONS3"
exit 0
