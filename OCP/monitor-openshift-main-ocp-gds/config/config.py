
import logging
import os
import string
from dotenv import load_dotenv
from kubernetes import client
import urllib3

def load_config():
    load_dotenv()
    API_TOKEN = os.getenv("API_TOKEN")
    HOST = os.getenv("HOST")
    configuration = client.Configuration()

    configuration.api_key["authorization"] = "Bearer {}".format(API_TOKEN)
    configuration.verify_ssl = False
    configuration.debug = False
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    configuration.host = HOST
    return configuration

def log_handler(filename: string):
    logger = logging.getLogger(filename)
    file_handler = logging.FileHandler("{}.log".format(filename))
    file_handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(message)s"))
    file_handler.setLevel(logging.INFO)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(message)s"))
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    return logger