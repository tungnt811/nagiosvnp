import json
import string
from kubernetes import client
from kubernetes.client.rest import ApiException
import config
import sys, time
from datetime import datetime

CONFIGURATION = None
STATUS = ["OK", "WARNING", "CRITICAL"]

DATE_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

def check_config():
    global CONFIGURATION
    if CONFIGURATION == None:
        CONFIGURATION = config.load_config()

def list_pod_namespace(namespace: string):
    global CONFIGURATION
    check_config()
    with client.ApiClient(CONFIGURATION) as api_client:
        api_instance = client.CoreV1Api(api_client)
        try:
            response = api_instance.list_namespaced_pod(namespace)
            return response
        except ApiException as e:
            sys.stdout.write("Error get pod: {}\n".format(e))
            sys.stdout.flush()
            time.sleep(0.01)

def list_pod_with_label(namespace: string, labels: string):
    global CONFIGURATION
    check_config()
    with client.ApiClient(CONFIGURATION) as api_client:
        api_instance = client.CoreV1Api(api_client)
        try:
            res = api_instance.list_namespaced_pod(namespace, label_selector=labels)
            return res.items
        except ApiException as e:
            sys.stdout.write("Error get pod: {}\n".format(e))
            sys.stdout.flush()
            time.sleep(0.01)

def read_pod_status(pod: client.V1Pod, check_time: int):
    global CONFIGURATION
    check_config()
    is_false = 0
    is_restart = 0
    containers = pod.status.container_statuses
    for c in containers:
        if not c.ready:
            is_false = 1
            return is_restart, is_false
        if c.restart_count > 0:
            startAt = c.state.running.started_at
            startAt = startAt.replace(tzinfo=None)
            nowTime = datetime.utcnow()
            delta = (nowTime - startAt).total_seconds()
            if delta <= check_time:
                is_restart = 1
                return is_restart, is_false
    return is_restart, is_false            

def check_usage_pod(pod: client.V1Pod, mode: string, warning: float, critical: float):
    global CONFIGURATION
    check_config()
    if pod.status.phase == "Running":
        with client.ApiClient(CONFIGURATION) as api_client:
            try:
                res = api_client.call_api('/apis/metrics.k8s.io/v1beta1/namespaces/' + pod.metadata.namespace + '/pods/' + pod.metadata.name, 'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
                pod_data = json.loads(res[0].data.decode('utf-8'))
                return check_container_resource(pod, pod_data["containers"], mode, warning, critical)
            except ApiException as e:
                sys.stdout.write("Check pod: {} critical: {}\n".format(pod.metadata.name, e.reason))
                sys.stdout.flush()
                time.sleep(0.01)
                return 2
    else:
        sys.stdout.write("Pod isn't running\n")
        sys.stdout.flush()
        time.sleep(0.01)
        return 2

def check_container_resource(pod: client.V1Pod, usage, mode: string, warning: float, critical: float):
    global CONFIGURATION
    check_config()
    container = pod.spec.containers
    MaxStatus = 0
    for c in container:
        status = 0
        if c.resources.limits == None:
            sys.stdout.write("Pod: {} container: {} not define resources limit\n".format(pod.metadata.name, c.name))
            sys.stdout.flush()
            time.sleep(0.01)
            continue
        for uc in usage:
            try:
                if c.name == uc["name"]:
                    cpu_limit, mem_limit = convert_resource_limit(c.resources.limits)
                    cpu_usage, mem_usage = convert_resource_limit(uc["usage"])
                    # mem_usage = int(uc["usage"]["memory"].replace("Ki", ""))
                    cpu_percentage = round((cpu_usage / cpu_limit) * 100.0, 2)
                    mem_percentage = round((mem_usage / mem_limit) * 100.0, 2)
                    if mode == "cpu":
                        if cpu_percentage >= warning:
                            status = 1
                        if cpu_percentage >= critical:
                            status = 2
                        sys.stdout.write("{} - Pod: {} container: {} CPU usage:{}% | cpu_usage={}\n".format(STATUS[status], pod.metadata.name, c.name, cpu_percentage, cpu_percentage))
                        sys.stdout.flush()
                        time.sleep(0.01)
                    if mode == "mem":
                        if mem_percentage >= warning:
                            status = 1
                        if mem_percentage >= critical:
                            status = 2
                        sys.stdout.write("{} - Pod: {} container: {} MEM usage:{}% | mem_usage={}\n".format(STATUS[status], pod.metadata.name, c.name, mem_percentage, mem_percentage))
                        sys.stdout.flush()
                        time.sleep(0.01)
                MaxStatus = max(status, MaxStatus)
            except AttributeError as e:
                sys.stdout.write("Error check container resource: {}\n".format(e))
                sys.stdout.flush()
                time.sleep(0.01)
                MaxStatus = 2
    return MaxStatus

def convert_resource_limit(resource):
    if "m" in resource["cpu"]:
        cpu = int(resource["cpu"].replace("m", ""))
    elif "n" in resource["cpu"]:
        cpu = int(resource["cpu"].replace("n", "")) / (1000*1000)
    else:
        cpu = int(resource["cpu"]) * 1000
    if "Ki" in resource["memory"]:
        mem = int(resource["memory"].replace("Ki", ""))
    if "Mi" in resource["memory"]:
        mem = int(resource["memory"].replace("Mi", "")) * 1024
    if "Gi" in resource["memory"]:
        mem = int(resource["memory"].replace("Gi", "")) * 1024 * 1024
    return cpu, mem
    

def monitor_all_pod_namespace(namespace: string, time_check: int):
    check_config()
    restart_count = 0
    false_count = 0
    pods = list_pod_namespace(namespace)
    for pod in pods.items:
        is_restart, is_false = read_pod_status(pod, time_check)
        if is_restart > 0:
            sys.stdout.write("WARNING - Pod: {} is recently restart\n".format(pod.metadata.name))
            sys.stdout.flush()
            time.sleep(0.01)
        if is_false > 0:
            sys.stdout.write("CRITICAL - Pod: {} is not ready\n".format(pod.metadata.name))
            sys.stdout.flush()
            time.sleep(0.01)
        restart_count += is_restart
        false_count += is_false
    return restart_count, false_count