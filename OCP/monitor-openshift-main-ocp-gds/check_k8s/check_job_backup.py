import config
from kubernetes import client
import logging
import sys
from kubernetes.client.rest import ApiException

def list_pod_namespace(namespace):
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s',
        handlers=[
            logging.FileHandler("pod.log"),
            logging.StreamHandler(sys.stdout)
        ]
    )
    configuration = config.load_config()
    with client.ApiClient(configuration) as api_client:
        api_instance = client.CoreV1Api(api_client)
        try:
            response = api_instance.list_namespaced_pod(namespace)
            return response
        except ApiException as e:
            logging.error("Error get pod: {}".format(e))