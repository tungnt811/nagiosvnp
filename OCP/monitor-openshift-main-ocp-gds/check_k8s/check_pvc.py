
import json
import string
from kubernetes import client
from kubernetes.client.rest import ApiException
from .check_ready_node import list_node_name
import config
import sys, time

CONFIGURATION = None
STATUS = ["OK", "WARNING", "CRITICAL"]

def check_config():
    global CONFIGURATION
    if CONFIGURATION == None:
        CONFIGURATION = config.load_config()

def check_pvc_namespaced(namespace: string, warning: float, critical: float):
    check_status = 0
    pvc_count = 0
    warning_count = 0
    critical_count = 0
    global CONFIGURATION
    check_config()
    items = list_node_name()
    with client.ApiClient(CONFIGURATION) as api_client:
        for it in items:
            res = api_client.call_api("/api/v1/nodes/" + it + "/proxy/stats/summary", 'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            pods = json.loads(res[0].data.decode('utf-8'))["pods"]
            for pod in pods:
                if "volume" in pod:
                    for v in pod["volume"]:
                        if "pvcRef" in v and v["pvcRef"]["namespace"] == namespace:
                            pvc_count += 1
                            volume_bytes_capacity = int(v["capacityBytes"])
                            volume_bytes_used = int(v["usedBytes"])
                            volume_inodes_used = int(v["inodesUsed"])
                            volume_inodes_capacity = int(v["inodes"])
                            volume_bytes_utilization = 100.0 * volume_bytes_used / volume_bytes_capacity
                            volume_inodes_utilization = 100.0 * volume_inodes_used / volume_inodes_capacity
                            
                            if volume_bytes_utilization > critical:
                                sys.stdout.write("CRITICAL - High storage utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_bytes_utilization, 2), volume_bytes_capacity))
                                check_status = max(check_status, 2)
                                sys.stdout.flush()
                                time.sleep(0.01)
                                critical_count += 1
                            elif volume_bytes_utilization > warning:
                                sys.stdout.write("WARNING - High storage utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_bytes_utilization, 2), volume_bytes_capacity))
                                check_status = max(check_status, 1)
                                sys.stdout.flush()
                                time.sleep(0.01)
                                warning_count += 1

                            if volume_inodes_utilization > critical:
                                sys.stdout.write("CRITICAL - High inodes utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_inodes_utilization, 2), volume_bytes_capacity))
                                check_status = max(check_status, 2)
                                sys.stdout.flush()
                                time.sleep(0.01)
                                critical_count += 1
                            elif volume_inodes_utilization > warning:
                                sys.stdout.write("WARNING - High inodes utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_inodes_utilization, 2), volume_bytes_capacity))
                                check_status = max(check_status, 1)
                                sys.stdout.flush()
                                time.sleep(0.01)
                                warning_count += 1
    sys.stdout.write("{} - Check {} PVC in namespace {}, Warning: {}, Critical: {} \n".format(STATUS[check_status], pvc_count, namespace, warning_count, critical_count))
    sys.stdout.flush()
    time.sleep(0.01)
    return check_status


def check_pvc_name(namespace: string, pvc: string, warning: float, critical: float):
    check_status = 0
    global CONFIGURATION
    check_config()
    items = list_node_name()
    with client.ApiClient(CONFIGURATION) as api_client:
        for it in items:
            res = api_client.call_api("/api/v1/nodes/" + it + "/proxy/stats/summary", 'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            pods = json.loads(res[0].data.decode('utf-8'))["pods"]
            for pod in pods:
                if "volume" in pod:
                    for v in pod["volume"]:
                        if "pvcRef" in v and v["pvcRef"]["namespace"] == namespace and v["pvcRef"]["name"] == pvc:
                            volume_bytes_capacity = int(v["capacityBytes"])
                            volume_bytes_used = int(v["usedBytes"])
                            volume_inodes_used = int(v["inodesUsed"])
                            volume_inodes_capacity = int(v["inodes"])
                            volume_bytes_utilization = 100.0 * volume_bytes_used / volume_bytes_capacity
                            volume_inodes_utilization = 100.0 * volume_inodes_used / volume_inodes_capacity
                            
                            if volume_bytes_utilization > critical:
                                sys.stdout.write("CRITICAL - High storage utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_bytes_utilization, 2), volume_bytes_capacity))
                                sys.stdout.flush()
                                time.sleep(0.01)
                                check_status = max(check_status, 2)
                            elif volume_bytes_utilization > warning:
                                sys.stdout.write("WARNING - High storage utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_bytes_utilization, 2), volume_bytes_capacity))
                                sys.stdout.flush()
                                time.sleep(0.01)
                                check_status = max(check_status, 1)

                            if volume_inodes_utilization > critical:
                                sys.stdout.write("CRITICAL - High inodes utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_inodes_utilization, 2), volume_bytes_capacity))
                                sys.stdout.flush()
                                time.sleep(0.01)
                                check_status = max(check_status, 2)
                            elif volume_inodes_utilization > warning:
                                sys.stdout.write("WARNING - High inodes utilization on pvc {}(namespace: {}) usage: {}% \n".format(v["name"], namespace, round(volume_inodes_utilization, 2), volume_bytes_capacity))
                                sys.stdout.flush()
                                time.sleep(0.01)
                                check_status = max(check_status, 1)
    if check_status == 0:
        sys.stdout.write("OK - pvc {}(namespace: {}) usage: {}% \n".format(pvc, namespace, round(volume_bytes_utilization, 2), volume_bytes_capacity))
        sys.stdout.flush()
        time.sleep(0.01)
    return check_status