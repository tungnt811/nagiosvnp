import json
import string
from kubernetes import client
import config
import sys, time
from .check_ready_node import list_node_name
from .check_pods import convert_resource_limit

CONFIGURATION = None
STATUS = ["OK", "WARNING", "CRITICAL"]

def check_config():
    global CONFIGURATION
    if CONFIGURATION == None:
        CONFIGURATION = config.load_config()

def check_nodes_cpu(mode: string, warning: float, critical: float):
    check_status = 0
    warning_count = 0
    critical_count = 0
    global CONFIGURATION
    check_config()
    items =  list_node_name()
    with client.ApiClient(CONFIGURATION) as api_client:
        for it in items:
            res = api_client.call_api("/apis/metrics.k8s.io/v1beta1/nodes/" + it, 'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            usage_cpu, usage_mem = convert_resource_limit(json.loads(res[0].data.decode('utf-8'))["usage"])
            res = api_client.call_api("/api/v1/nodes/" + it, 'GET', auth_settings = ['BearerToken'], response_type='json', _preload_content=False)
            capacity_cpu, capacity_mem = convert_resource_limit(json.loads(res[0].data.decode('utf-8'))["status"]["capacity"])
            allocatable_cpu, allocatable_mem = convert_resource_limit(json.loads(res[0].data.decode('utf-8'))["status"]["allocatable"])
            
            usage_cpu_per = round((usage_cpu / capacity_cpu) * 100.0, 2)
            usage_mem_per = round((usage_mem / capacity_mem) * 100.0, 2)
            allocatable_cpu_per = 100 - round((allocatable_cpu / capacity_cpu) * 100.0, 2)
            allocatable_mem_per = 100 - round((allocatable_mem / capacity_mem) * 100.0, 2)
            if mode == "node-cpu":
                status = 0
                if usage_cpu_per >= critical:
                    status = 2
                    critical_count += 1
                elif usage_cpu_per >= warning:
                    status = 1
                    warning_count += 1
                if status > 0:
                    sys.stdout.write("{} - Node: {} CPU usage:{}% | cpu_usage={}\n".format(STATUS[status], it, usage_cpu_per, usage_cpu_per))
                    sys.stdout.flush()
                    time.sleep(0.01)
                check_status = max(check_status, status)

                status = 0
                if allocatable_cpu_per >= critical:
                    status = 2
                    critical_count += 1
                elif allocatable_cpu_per >= warning:
                    status = 1
                    warning_count += 1
                if status > 0:
                    sys.stdout.write("{} - Node: {} CPU allocatable:{}% | cpu_allocatable={}\n".format(STATUS[status], it, allocatable_cpu_per, allocatable_cpu_per))
                    sys.stdout.flush()
                    time.sleep(0.01)
                check_status = max(check_status, status)
            if mode == "node-mem":
                status = 0
                if usage_mem_per >= critical:
                    status = 2
                    critical_count += 1
                elif usage_mem_per >= warning:
                    status = 1
                    warning_count += 1
                if status > 0:
                    sys.stdout.write("{} - Node: {} MEM usage:{}% | mem_usage={}\n".format(STATUS[status], it, usage_mem_per, usage_mem_per))
                    sys.stdout.flush()
                    time.sleep(0.01)
                check_status = max(check_status, status)

                status = 0
                if allocatable_mem_per >= critical:
                    status = 2
                    critical_count += 1
                elif allocatable_mem_per >= warning:
                    status = 1
                    warning_count += 1
                if status > 0:
                    sys.stdout.write("{} - Node: {} MEM allocatable:{}% | mem_allocatable={}\n".format(STATUS[status], it, allocatable_mem_per, allocatable_mem_per))
                    sys.stdout.flush()
                    time.sleep(0.01)
                check_status = max(check_status, status)
    sys.stdout.write("{} - Check Node: Warning: {}, Critical: {}\n".format(STATUS[check_status], warning_count, critical_count))
    sys.stdout.flush()
    time.sleep(0.01)
    return check_status