import config
from kubernetes import client
import logging
import sys
from kubernetes.client.rest import ApiException

CONFIGURATION = None
STATUS = ["OK", "WARNING", "CRITICAL"]

def check_config():
    global CONFIGURATION
    if CONFIGURATION == None:
        CONFIGURATION = config.load_config()

def list_node_name():
    global CONFIGURATION
    check_config()
    with client.ApiClient(CONFIGURATION) as api_client:
        api_instance = client.CoreV1Api(api_client)
        try:
            nodes = api_instance.list_node()
            nodes = client.V1NodeList("v1", nodes.items)
            
            return [ node.metadata.name for node in nodes.items ]
        except ApiException as e:
            return []

def check_status_node():
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s',
        handlers=[
            logging.FileHandler("node.log"),
            logging.StreamHandler(sys.stdout)
        ]
    )
    configuration = config.load_config()
    with client.ApiClient(configuration) as api_client:
        api_instance = client.CoreV1Api(api_client)
        try:
            nodes = api_instance.list_node()
            nodes = client.V1NodeList("v1", nodes.items)
            
            for node in nodes.items:
                node_metadata = node.metadata
                node_status = client.V1Status(node.status)
                for condition in node_status.api_version.conditions:
                    log_level, check_result = format_logging(condition)
                    logging.log(log_level, "Node={} {}".format(node_metadata.name, check_result))
        except ApiException as e:
            logging.log(logging.ERROR, "Can't get list node %s" % e)

def format_logging(condition: client.V1NodeCondition):
    check_result = "{}={} message={} reason={}\n".format(condition.type, condition.status, condition.message, condition.message)
    if condition.type != "Ready":
        if condition.status == "True":
            return logging.CRITICAL, check_result
        return logging.INFO, check_result
    if condition.status == "True":
        return logging.INFO, check_result
    return logging.CRITICAL, check_result