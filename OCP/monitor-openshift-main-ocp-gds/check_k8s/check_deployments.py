import string
from kubernetes import client
from kubernetes.client.rest import ApiException
import logging
import sys
import config, time

def get_pod_template(name: string, namespace: string):
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s',
        handlers=[
            logging.FileHandler("deployment-{}-{}.log".format(name, namespace)),
            logging.StreamHandler(sys.stdout)
        ]
    )
    configuration = config.load_config()
    with client.ApiClient(configuration) as api_client:
        api_instance = client.AppsV1Api(api_client)
        try:
            response = api_instance.read_namespaced_deployment_status(name, namespace)
            if response.spec.template != None:
                sys.stdout.write("Find deployment\n")
                sys.stdout.flush()
                time.sleep(0.01)
                return client.V1PodTemplate(response.spec.template)
        except ApiException as e:
            logging.error("Error get deployment: {} in namespace: {} message: {}".format(name, namespace, e))

def get_pod_deployment(name: string, namespace: string):
    pass

def get_event_deployment(name: string, namespace: string):
    pass