# monitor-openshift
oc -n loyalty-bussiness-prd get secret nagios-token-qgl7r  -o jsonpath={.data.token} | base64 -d
Requirement:
 + Python 
 + Pip
 
Install requirements:
 + pip3 install -r requirements.txt

Test:
 + CPU:
   ./test -n loyalty-system-throughput -m cpu -l peer-id=appclient0.org1.bidv.com -w 40 -c 60
 + MEM:
   ./test -n loyalty-system-throughput -m mem -l peer-id=appclient0.org1.bidv.com -w 40 -c 60
 + PVC in namespace:
   ./test -n loyalty-system-throughput -m pvc -w 40 -c 60
 + PVC name:
   ./test -n loyalty-system-throughput -P nfs-pvc -m pvc -w 80 -c 90