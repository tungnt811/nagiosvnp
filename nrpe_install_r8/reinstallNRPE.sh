#!/bin/bash

 # function helpMe(){
     # # output standard usage
     # echo -e "Usage: `basename $0` nagios_serv_private_ip";
 # }
 # check user args
 # if [[ $# == 0 ]]; then
     # helpMe;
     # exit;
 # else
     # master_priv_ip=$1;
 # fi

function uninstall3() {
    FILE=/usr/local/nagios/bin/nrpe-uninstall;
    if [[ -f "$FILE" ]]; then
	   cd /usr/local/nagios/bin;
	   sudo ./nrpe-uninstall all;
	else
	   echo "$FILE does not exist.";
	fi
}
# install nrpe
function removeNRPE() {
    rm -f /etc/xinetd.d/nrpe
    rm -f /etc/init/nrpe.conf
    rm -f /usr/local/nagios/bin/nrpe*
    rm -f /usr/local/nagios/etc/nrpe*
    rm -f /usr/local/nagios/libexec/*nrpe*
    rm -rf /usr/local/nagios/libexec/
    rm -rf /usr/local/nagios/etc/nrpe/
}
function repo() {
    cd /tmp/nrpe_install_r8/
	cp repomini8.tar.gz /opt
	cd /opt
	tar -zxvf repomini8.tar.gz
	mkdir -p /etc/yum.repos.d/backup/
    mv /etc/yum.repos.d/*.repo  /etc/yum.repos.d/backup/
	cp /tmp/nrpe_install_r8/repomini.repo /etc/yum.repos.d/

}
function installNRPE() {
	# useradd nagios
    # usermod -aG nagios nagios
    # chage -I -1 -m 0 -M 99999 -E -1 nagios
    # echo -e "Vanhanh@123\nVanhanh@123" | passwd nagios
    cd /tmp/nrpe_install_r8/
    rpm -ivh nagios-common-4.4.6-4.el8.x86_64.rpm 
    rpm -ivh nrpe-4.0.3-6.el8.x86_64.rpm 
	rpm -ivh perl-File-ReadBackwards-1.05-11.el8.noarch.rpm
    rpm -ivh nagios-plugins-*
	cd /tmp/nrpe_install_r8/semanageRH83
	yum install -y *.rpm
    #sed -i 's/allowed_hosts=127.0.0.1,.../allowed_hosts=127.0.0.1,10.22.111.60,10.22.111.61,10.22.111.62,10.22.111.68,10.22.111.77,10.22.111.57,10.22.111.58,10.22.111.59/g' /usr/local/nagios/etc/nrpe.cfg
	sed -i '/^allowed_hosts=/s/$/,10.0.171.201/' /usr/local/nagios/etc/nrpe.cfg
    sed -i 's/dont_blame_nrpe=0/dont_blame_nrpe=1/g' /usr/local/nagios/etc/nrpe.cfg
    echo 'include_dir=/usr/local/nagios/etc/nrpe' >> /usr/local/nagios/etc/nrpe.cfg
    systemctl enable nrpe
    systemctl start nrpe.service
    mkdir -p /usr/local/nagios/etc/nrpe/
    chown -R nagios: /usr/local/nagios/libexec/
    chown -R nagios: /usr/local/nagios/etc/nrpe/
    chown -R nagios: /usr/local/nagios/
    chmod -R 775 /usr/local/nagios/libexec/
    chmod -R 775 /usr/local/nagios/etc/nrpe/
    semanage permissive -l
    semanage permissive -a nrpe_t
    echo 'nrpe:ALL' >> /etc/hosts.allow
	echo 'nrpe ALL=NOPASSWD: /usr/local/nagios/libexec/* *' >> /etc/sudoers
    echo 'nrpe ALL=NOPASSWD: /usr/local/nagios/* *' >> /etc/sudoers
    sudo systemctl restart nrpe
}
function usermod() {
    echo 'nagios ALL=NOPASSWD: /usr/local/nagios/libexec/* *' >> /etc/sudoers
	echo 'nagios ALL=NOPASSWD: /usr/local/nagios/* *' >> /etc/sudoers
    echo 'nagios ALL=NOPASSWD: /etc/nrpe/* *' >> /etc/sudoers
    echo 'nagios ALL=(root) NOPASSWD: /bin/systemctl status nrpe' >> /etc/sudoers
    echo 'nagios ALL=(root) NOPASSWD: /bin/systemctl start nrpe' >> /etc/sudoers
    echo 'nagios ALL=(root) NOPASSWD: /bin/systemctl stop nrpe' >> /etc/sudoers
    echo 'nagios ALL=(root) NOPASSWD: /bin/systemctl restart nrpe' >> /etc/sudoers
	echo 'nrpe:ALL' >> /etc/hosts.allow
	echo 'nrpe ALL=NOPASSWD: /usr/local/nagios/libexec/* *' >> /etc/sudoers
    echo 'nrpe ALL=NOPASSWD: /usr/local/nagios/* *' >> /etc/sudoers
	# sed -i '/^allowed_hosts=/s/$/,10.20.8.17/' /usr/local/nagios/etc/nrpe.cfg
    sudo systemctl restart nrpe
}
function copy_file() {
    cd /tmp/nrpe_install_r8/files;
    cp -r vnpay.cfg vnpay_linux.cfg /usr/local/nagios/etc/nrpe/;
    cp -r check_linux_stats.pl check_time_diff.sh count_connection_port.sh utils.pm check_cpu_linux.sh check_tcp_count_connection.sh /usr/local/nagios/libexec/;
    chmod -R 775 /usr/local/nagios/libexec/;
    chmod -R 775 /usr/local/nagios/etc/nrpe/;
	chown -R nagios: /usr/local/nagios/libexec/
    chown -R nagios: /usr/local/nagios/etc/nrpe/
	sudo systemctl restart nrpe
}
repo;
#uninstall3;
#removeNRPE;
 installNRPE;
 copy_file;
 usermod;
