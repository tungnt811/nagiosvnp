#!/bin/sh
# Nagios return codes
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
PORT=20145
IP=''
WARNING_THRESHOLD=1500
CRITICAL_THRESHOLD=2000
WARNC_THRESHOLD=100
CRITC_THRESHOLD=150
NO_CONN=0
CHK_TCP=''
# Parse parameters
while [ $# -gt 0 ]; do
    case "$1" in
                -p | --port)
                shift
                PORT=$1
                ;;
        -h | --ip)
                shift
                IP=$1
                ;;
        -n | --non)
                shift
                NO_CONN=1
                ;;
        -w | --warning)
                shift
                WARNING_THRESHOLD=$1
                ;;
		-c | --critical)
                shift
                CRITICAL_THRESHOLD=$1
                ;;
		-wc | --warnc)
                shift
                WARNC_THRESHOLD=$1
                ;;
		-cc | --critc)
                shift
                CRITC_THRESHOLD=$1
                ;;
        *)  echo "Unknown argument: $1"
            exit $STATE_UNKNOWN
            ;;
        esac
shift
done


#Check tcp port
tcp_temp=$(timeout 1 bash -c "echo 'check port $port' >/dev/tcp/$IP/$PORT" 2>&1)
rc=$?
EXIT_CODE=$STATE_OK
if [ $rc == 0 ]; then
	CHK_TCP=''
elif [ $rc == 124 ]; then
	CHK_TCP="TCP Timeout - connect $IP port $PORT, "
	EXIT_CODE=$STATE_CRITICAL
else
	CHK_TCP=$'TCP Error -'$(echo $tcp_temp | sed -E 's/(^bash:)//' | sed -E 's/(bash:.*)//')', '
	EXIT_CODE=$STATE_WARNING
fi


#Get port connection
CONNECTIONS1=(`netstat -ntu | grep :$PORT | grep 'ESTABLISHED' | wc -l`)
CONNECTIONS2=(`netstat -ntu | grep :$PORT | grep 'TIME_WAIT' | wc -l`)
CONNECTIONS3=(`netstat -ntu | grep :$PORT | grep 'CLOSE_WAIT' | wc -l`)

if  [ $CONNECTIONS1 -ge $CRITICAL_THRESHOLD ] || [ $CONNECTIONS3 -ge $CRITC_THRESHOLD ]; then
	LIST=$(netstat -ant | awk '{print $4":"$5":"$6}' |grep :$PORT|grep "ESTABLISHED\|WAIT"| awk 'BEGIN { FS=":" } /1/ {print $3":"$5}' | sort | uniq -c | sort -nr)
	MSG=$'\n'$LIST
	echo $CHK_TCP"Very many connection, ESTABLISHED=$CONNECTIONS1, TIME_WAIT=$CONNECTIONS2, CLOSE_WAIT=$CONNECTIONS3 $MSG| 'Established'=$CONNECTIONS1;$WARNING_THRESHOLD;$CRITICAL_THRESHOLD;0;0, 'Time_Wait'=$CONNECTIONS2;0;0, 'Close_Wait'=$CONNECTIONS3;$WARNC_THRESHOLD;$CRITC_THRESHOLD;0;0"
	exit $STATE_CRITICAL
elif  [ $CONNECTIONS1 -ge $WARNING_THRESHOLD ] || [ $CONNECTIONS3 -ge $WARNC_THRESHOLD ]; then
	LIST=$(netstat -ant | awk '{print $4":"$5":"$6}' |grep :$PORT|grep "ESTABLISHED\|WAIT"| awk 'BEGIN { FS=":" } /1/ {print $3":"$5}' | sort | uniq -c | sort -nr)
	MSG=$'\n'$LIST
	echo $CHK_TCP"Many connection, ESTABLISHED=$CONNECTIONS1, TIME_WAIT=$CONNECTIONS2, CLOSE_WAIT=$CONNECTIONS3 $MSG| 'Established'=$CONNECTIONS1;$WARNING_THRESHOLD;$CRITICAL_THRESHOLD;0;0, 'Time_Wait'=$CONNECTIONS2;0;0, 'Close_Wait'=$CONNECTIONS3;$WARNC_THRESHOLD;$CRITC_THRESHOLD;0;0"
	exit $STATE_WARNING
elif [ $CONNECTIONS1 -eq 0 ] && [ $NO_CONN == 1 ]; then
	echo $CHK_TCP"No connection, ESTABLISHED=$CONNECTIONS1, TIME_WAIT=$CONNECTIONS2, CLOSE_WAIT=$CONNECTIONS3 | 'Established'=$CONNECTIONS1;$WARNING_THRESHOLD;$CRITICAL_THRESHOLD;0;0, 'Time_Wait'=$CONNECTIONS2;0;0, 'Close_Wait'=$CONNECTIONS3;$WARNC_THRESHOLD;$CRITC_THRESHOLD;0;0"
	exit $STATE_WARNING
elif [ ${#CHK_TCP} -gt 0 ]; then
	echo $CHK_TCP"ESTABLISHED=$CONNECTIONS1, TIME_WAIT=$CONNECTIONS2, CLOSE_WAIT=$CONNECTIONS3 | 'Established'=$CONNECTIONS1;$WARNING_THRESHOLD;$CRITICAL_THRESHOLD;0;0, 'Time_Wait'=$CONNECTIONS2;0;0, 'Close_Wait'=$CONNECTIONS3;$WARNC_THRESHOLD;$CRITC_THRESHOLD;0;0"
	exit $EXIT_CODE
else
	echo "Normal, ESTABLISHED=$CONNECTIONS1, TIME_WAIT=$CONNECTIONS2, CLOSE_WAIT=$CONNECTIONS3 $MSG| 'Established'=$CONNECTIONS1;$WARNING_THRESHOLD;$CRITICAL_THRESHOLD;0;0, 'Time_Wait'=$CONNECTIONS2;0;0, 'Close_Wait'=$CONNECTIONS3;$WARNC_THRESHOLD;$CRITC_THRESHOLD;0;0"
	exit $STATE_OK
fi

