#!/bin/bash
#
# Written by: Robin Gierse - info@thorian93.de - on 20191016
#
# Purpose:
# This script backs up grafana database and configuration
#
# Version: 0.1 on 20191017
# Version: 0.2 on 20191022 - Add cleanup routine and switch compression to bz2
# Version: 0.3 on 20191029 - Repair cleanup routine
# Version: 1.0 on 20191107 - Change logfile to work with logrotate. Also bump version to 1.0 as the script works as intended.
#
# Usage:
# ./backup-grafana.sh

# Variables:

grafana_logfile="/var/log/backup/$(date +%Y%m%d_%H%M%S)_grafana_backup.log"
grafana_backup_root="/backup/grafana"
grafana_backup_dest="${grafana_backup_root}/$(date +%Y%m%d)"
grafana_backup_retention="7" # Delete backups older than number of days

# Functions:

_initialize() {
    echo "$(date): Starting Grafana Backup."
    touch "${grafana_logfile}"
    mkdir "${grafana_backup_dest}"
}

_grafana_backup() {
    echo "$(date) Backing up Grafana Configuration:"
#    tar -caf "${grafana_backup_dest}/grafana_conf.tar.bz2" /etc/grafana
    tar -zcf "${grafana_backup_dest}/grafana_conf.tar.gz" /etc/grafana
    echo "$(date) Backing up Grafana Database:"
#    tar -caf "${grafana_backup_dest}/grafana_db.tar.bz2" /var/lib/grafana/
    tar -zcf "${grafana_backup_dest}/grafana_db.tar.gz" /var/lib/grafana/
    echo "$(date) Backup done."
}

_grafana_cleanup() {
    echo 'Deleting old backups:'
    rm -rvf $(find ${grafana_backup_root} -mtime +${grafana_backup_retention})
    echo "$(date) Cleanup done."
}

_finalize() {
    echo "$(date): Finished Grafana Backup."
    exit 0
}

# Main
_initialize >> "${grafana_logfile}" 2>&1
_grafana_backup >> "${grafana_logfile}" 2>&1
_grafana_cleanup >> "${grafana_logfile}" 2>&1
_finalize >> "${grafana_logfile}" 2>&1