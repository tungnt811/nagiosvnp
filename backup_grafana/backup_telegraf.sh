#! /bin/sh   
hostname=$(hostname -I | awk '{print $1}')
date=$(date +"%d-%m-%y")
##copy thu muc can backup ra /home/nagios (hoac thu muc tuy y)
cp -r /etc/telegraf /home/nagios/
#cp -r /etc/logstash /home/nagios/
cp -r /etc/prometheus /home/nagios/
##di chyen den thu muc backup
cd /home/nagios/
##nen cac thu muc can backup lai
tar -czf /home/nagios/telegraf-$hostname-$date.tar.gz telegraf
#tar -czf /home/nagios/logstash-$hostname-$date.tar.gz logstash
tar -czf /home/nagios/prometheus-$hostname-$date.tar.gz prometheus
###file bk : telegraf-10.20.8.27-11-03-24.tar.gz
##xoa thu muc backup cu
rm -rf /home/nagios/prometheus
#rm -rf /home/nagios/logstash
rm -rf /home/nagios/telegraf

