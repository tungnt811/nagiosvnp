#!/bin/bash
#
# Written by: Robin Gierse - info@thorian93.de - on 20191016
#
# Purpose:
# This script backs up influx databases
#
# Version: 0.1 on 20191016
# Version: 0.2 on 20191017
# Version: 0.3 on 20191022 - Add cleanup routine and switch compression to bz2
# Version: 0.4 on 20191029 - Repair cleanup routine
# Version: 1.0 on 20191107 - Change logfile to work with logrotate. Also bump version to 1.0 as the script works as intended.
#
# Usage:
# ./backup-influxdb.sh

# Variables:

influxdb_logfile="/var/log/backup/$(date +%Y%m%d_%H%M%S)_influx_backup.log"
influxdb_databases='OMNI'
influxdb_host='localhost'
influxdb_backup_root="/backup/influxdb"
influxdb_backup_dest="${influxdb_backup_root}/$(date +%Y%m%d)/"
influxdb_backup_retention="7" # Delete backups older than number of days

# Functions:

_initialize() {
    echo "$(date): Starting InfluxDB Backup."
    touch "${influxdb_logfile}"
}

_influxdb_backup() {
    for influxdb_database in ${influxdb_databases}
    do
        echo "$(date) Backing up ${influxdb_database}:"
        influxd backup -portable -database "${influxdb_database}" -host "${influxdb_host}:8088" "${influxdb_backup_dest}/${influxdb_database}"
        echo "$(date) Compressing Backup.."
#        tar -caf "${influxdb_backup_dest}/${influxdb_database}.tar.bz2" "${influxdb_backup_dest}/${influxdb_database}"
        tar -zcf "${influxdb_backup_dest}/${influxdb_database}.tar.gz" "${influxdb_backup_dest}/${influxdb_database}"		
        echo "$(date) Cleanup raw backup files.."
        rm -rf "${influxdb_backup_dest:?}/${influxdb_database}"
        echo "$(date) ${influxdb_database} done."
    done
}

_influxdb_cleanup() {
    echo 'Deleting old backups:'
    rm -rvf $(find ${influxdb_backup_root} -mtime +${influxdb_backup_retention})
    echo "$(date) Cleanup done."
}

_finalize() {
    echo "$(date): Finished InfluxDB Backup."
    exit 0
}

# Main
_initialize >> "${influxdb_logfile}" 2>&1
_influxdb_backup >> "${influxdb_logfile}" 2>&1
_influxdb_cleanup >> "${influxdb_logfile}" 2>&1
_finalize >> "${influxdb_logfile}" 2>&1
