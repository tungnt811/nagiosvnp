#!/bin/bash

 # function helpMe(){
     # # output standard usage
     # echo -e "Usage: `basename $0` nagios_serv_private_ip";
 # }
 # # check user args
 # if [[ $# == 0 ]]; then
     # helpMe;
     # exit;
 # else
     # master_priv_ip=$1;
 # fi

function uninstall3() {
    cd /usr/local/nagios/bin/
	sudo ./nrpe-uninstall all
}
#remove old repo
function rmold_repo() {
    cd /etc/yum.repos.d;
    for f in *.*; do mv $f `basename $f .repo`.bk; done;
}
#add repo
function addrepo() {
    cd /tmp/install_nrpe4_redhat78;
    sudo tar -zxvf repominiRH78.tar.gz;
    sudo cat > /etc/yum.repos.d/RHEL.repo <<EOF
[RHEL7]
name=RHEL7 Linux 7 x86_64
baseurl=file:///tmp/install_nrpe4_redhat78/opt/repomini7
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY
gpgcheck=0
enabled=1
EOF
    sudo yum clean all;
    sudo yum repolist;
}
# install nrpe

function installNRPE() {
    cd /tmp/install_nrpe_source;
    sudo tar -zxf nrpe-4.1.0.tar.gz;
    cd nrpe-4.1.0;
    ./configure --enable-command-args;
    make all;
    sudo make install-groups-users;
    sudo make install;
    sudo make install-config;
    echo >> /etc/services;
    echo '#Nagios services' >> /etc/services;
    echo 'nrpe    5666/tcp' >> /etc/services;
    sudo make install-init;
    sudo systemctl enable nrpe.service;
    sudo mkdir -p /usr/local/nagios/etc/nrpe;
    sed -i '/^allowed_hosts=/s/$/,10.20.8.17,10.20.8.15,10.20.8.16,10.20.8.11,10.20.8.12,10.20.8.18/' /usr/local/nagios/etc/nrpe.cfg
    sudo sed -i 's/^dont_blame_nrpe=.*/dont_blame_nrpe=1/g' /usr/local/nagios/etc/nrpe.cfg;
    sudo sed -i '$ a include_dir=/usr/local/nagios/etc/nrpe' /usr/local/nagios/etc/nrpe.cfg;
    sudo systemctl start nrpe.service;
}
function installPlugin() {
    cd /tmp/install_nrpe_source;
    sudo tar -zxf nagios-plugins-2.3.3.tar.gz;
    cd nagios-plugins-2.3.3;
    ./configure;
    make;
    sudo make install;
}
function copy_file() {
    cd /tmp/install_nrpe_source/files;
    sudo cp -r vnpay.cfg vnpay_linux.cfg /usr/local/nagios/etc/nrpe;
    sudo cp -r check_linux_stats.pl check_time_diff.sh count_connection_port.sh check_tcp_count_connection.sh check_cpu_linux.sh /usr/local/nagios/libexec;
    sudo chmod -R 775 /usr/local/nagios/libexec;
    sudo chmod -R 755 /usr/local/nagios/etc/nrpe;
}

function copy_lib() {
    cd /tmp/install_nrpe_source;
    sudo tar -zxf lib.tar.gz -C /usr/local/nagios/libexec;
}
uninstall3;
#rmold_repo;
#addrepo;
installNRPE;
installPlugin;
copy_file;
#copy_lib;

