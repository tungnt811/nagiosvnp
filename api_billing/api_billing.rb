#!/usr/bin/env ruby
# encoding: utf-8


require 'sinatra'
require 'json'
require 'date'
require 'influxdb'

set :run, true
set :sever, 'thin'
set :bind, '0.0.0.0'
set :port, 8083
set :environment, :production


provider = {'109200' => 'VIETNAMMOBILE', '109900' => 'GTEL', '109000' => 'MOBIFONE', '109100' => 'VINAPHONE', '109800' => 'VIETTEL', '103401' => 'MOBIDATA3G-4G_CARD', '103300' => 'VIETTEL_CARD', '102600' => 'ON_CARD', '103100' => 'VCOIN_CARD', '103700' => 'GARENA_CARD', '103200' => 'VINA_CARD', '103400' => 'MOBI_CARD', '104900' => 'SMART_CARD', '108500' => 'OP_CARD', '102800' => 'M_CARD', '102900' => 'VGOLD_CARD', '103500' => 'VIETNAM_CARD', '103310' => 'VIETTEL_CARD-NEW', '102000' => 'VCOINTOP_CARD', '103301' => 'VIETTELDATA3G-4G_CARD', '102300' => 'VINAGAME_CARD', '103000' => 'DEC_CARD', '102700' => 'A_CARD', '102400' => 'GATE_CARD', '102500' => 'E_CARD'}
partner = { '970401' => 'VNPAY01', '970402' => 'VNPAY02', '970404' => 'SEABANK', '970408' => 'VNPAY08', '970409' => 'VNPAY09', '970412' => 'PVCBank', '970415' => 'VIETINBANK2', '970418' => 'VIBANK', '970419' => 'AGIRBANK_MB', '970420' => 'DONGABANK', '970422' => 'TECHCOMBANK', '970423' => 'TIENPHONGBANK', '970424' => 'TPB_LO', '970425' => 'NAVIBANK', '970426' => 'LIENVIETBANK', '970427' => 'INDOVINABANK', '970429' => 'OCEANBANK', '970430' => 'SACOMBANK', '970431' => 'EXIMBANK', '970432' => 'ABBANK', '970433' => 'MSB', '970434' => 'VIDBANK', '970435' => 'MBBANK', '970436' => 'VIETCOMBANK', '970437' => 'HDBANK', '970438' => 'BAOVIETBANK', '970439' => 'NAMABANK', '970440' => 'MHB', '970443' => 'SHB', '970445' => 'OCB', '970446' => 'BIDC', '970448' => 'PaymentGateway', '970449' => 'VNMART', '970450' => 'VNMART_WS', '970451' => 'SDN_TT', '970452' => 'SDN_TS', '970453' => 'SimDaNang', '970454' => 'VCCB', '970455' => 'SCB', '970461' => 'SCBank', '970462' => 'LIENVIETBANKWEBTOPUP', '970466' => 'VPBANK', '970476' => 'ShinhanBank', '970478' => 'VietABank', '970482' => 'VIETINBANKWEB', '970488' => 'BIDV', '970489' => 'VIETINBANK', '970499' => 'AGRIBANK'}
channel = {'6011' => 'ATM','6012' => 'Teller','6013' => 'SMS','6014' => 'Internet Banking','6015' => 'Mobile Banking','6016' => 'UNC','6017' => 'OTHER'}
#####################################################

async_options = {
	# number of points to write to the server at once
	max_post_points:     1000,
	# queue capacity
	max_queue_size:      10_000,
	# number of threads
	num_worker_threads:  30,
	# max. time (in seconds) a thread sleeps before
	# checking if there are new jobs in the queue
	sleep_interval:      5,
	# whether client will block if queue is full
	block_on_full_queue: false
}
username = 'billing'
password = 'billing@123'
database = 'billing'
#url = 'http://influxdb.vnp.vnpaytest.local'
#influxdb = InfluxDB::Client.new database, username: username, password: password, url: url #, async: async_options
influxdb = InfluxDB::Client.new database, username: username, password: password
	
get '/' do
  "Hello this is API to post smsgw.resp!<br>API: http://10.22.11.11:8083/api_billing"
end

post "/api_billing" do
  logger.info "Billing Transaction"
  #{"EventDateTime"=>"2020 06 16 10 22 14", "ResponseCode"=>"0", "TransactionAmount"=>"35000", "CardScheme"=>"NAPAS", "MerchantCode"=>"60431582", "TerminalCode"=>"80165196"}
  request.body.rewind  # in case someone already read it
  post_data = request.body.read
  logger.info "Data post: #{post_data}"
begin
  data = JSON.parse post_data
  if (data.length > 0) 
    msg = to_influxdb(influxdb,data)
  else
    halt 400,'Bad Request, data not found'
  end
rescue JSON::ParserError
  msg = "Invalid json string: #{post_data}"
  logger.error post_data
end
  msg
end

def to_influxdb(influxdb, data)
	begin

	time_precision = 'ns'
	retention = '12hours'
	successed = 0
	failed = 0
	amount_successed = 0
	amount_failed = 0
	timeInProcess = data['timeInProcess']
	
	if ( data['respCode'] =~ /^0+$/ ) 
		successed = 1
	else
		failed = 1
	end
	#timeProcess =~ /^(d+)/
	influx_data = [
	  {
		series: 'billing',
		tags: {'requId' => data['requId'], 'serviceCode' => data['serviceCode'], 'processCode' => data['processCode'],'processName' => data['processName'],'partnerCode' => data['partnerCode'], 'providerCode' => data['providerCode'], 'productCode' => data['productCode'], 'request' => data['request'], 'recipient' => data['recipient'], 'debitAcountNo' => data['debitAcountNo'], 'channel' => data['channel'], 'referencePar' => data['referencePar'], 'trace' => data['trace'], 'currencyCode' => data['currencyCode'], 'queueResponse' => data['queueResponse'], 'serverDb' => data['serverDb'], 'respCode' => data['respCode'], 'errorMsg' => data['errorMsg'], 'nameServiceProces' => data['nameServiceProces'], 'datetimeTxn' => data['datetimeTxn'], 'partnerName' => data['partnerName'], 'providerName' => data['providerName'], 'productName' => data['productName']  },
		values: { 'requId' => data['requId'], 'timeInProcess'=> timeInProcess.to_i, 'amount'=> data['amount'], 'debitAmount'=> data['debitAmount'], 'successed'=> successed, 'failed'=> failed },
	  }
	]
	#logger.info influx_data
	influxdb.write_points(influx_data, time_precision,retention)
	rescue Exception => e
		logger.info e.message
	end
	return "done"
end




