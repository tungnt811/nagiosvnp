#!/usr/bin/env ruby
require 'daemons'
base_dir = File.dirname(__FILE__)

file = File.expand_path('api_billing.rb', base_dir)

options = {
  app_name: 'api_billing',
  dir_mode: :normal,
  dir: File.expand_path('', base_dir),
  monitor: true,
  log_dir: File.expand_path('', base_dir),
  log_output: true,
  backtrace: true,
  output_logfilename: "../../var/log/api_billing.log",
}
Daemons.run_proc(File.basename(file), options) do
  exec "ruby #{file}"
end
